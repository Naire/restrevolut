import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.service.AccountService;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class AccountTest {
    private static final String accountsPath = "accounts";
    private static final long exampleId = 1L;

    private AccountService accountService = mock(AccountService.class);
    private PrintWriter writer = mock(PrintWriter.class);
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private List<Account> exampleAllAccounts = new ArrayList<>();

    @Before
    public void setUp() throws InternalLogicException, DataNotFoundException, IOException {
        when(response.getWriter()).thenReturn(writer);
        Account account = Helper.getExampleAccount(Helper.getExamplePerson(exampleId), exampleId);
        Account account1 = Helper.getExampleAccount(Helper.getExamplePerson(exampleId), exampleId + 1);
        when(accountService.getAccount(anyLong())).thenReturn(account);
        exampleAllAccounts.add(account);
        exampleAllAccounts.add(account1);
        when(accountService.getAllAccounts()).thenReturn(exampleAllAccounts);
        when(accountService.getHistory(account.getId())).thenReturn(Helper.getExampleHistory(account));
        when(accountService.getHistory(account1.getId())).thenReturn(Helper.getExampleHistory(account1));
    }

    @Test
    public void shouldGetAllAccounts() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Account.class);
        TestServlet servlet = new TestServlet(accountService);
        servlet.setPath(accountsPath + "/");
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(exampleAllAccounts, result);
    }

    @Test
    public void shouldGetAccountById() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Account.class);
        TestServlet servlet = new TestServlet(accountService);
        Account account = exampleAllAccounts.get(0);
        servlet.setPath(accountsPath + "/" + account.getId());
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(account, result);
    }

    @Test
    public void shouldGetBalance() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Long.class);
        TestServlet servlet = new TestServlet(accountService);
        servlet.setPath(accountsPath + "/" + exampleId + "/balance");
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(Helper.exampleBalance, result);
    }

    @Test
    public void shouldGetHistory() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(String.class);
        TestServlet servlet = new TestServlet(accountService);
        Account account = exampleAllAccounts.get(0);
        servlet.setPath(accountsPath + "/" + account.getId() + "/history");
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(Helper.getExampleHistory(account).toString(), result.toString());
    }

    @WebServlet("/test")
    public static class TestServlet extends HttpServlet {
        private final AccountService accountService;
        private String path = "";

        @Inject
        public TestServlet(AccountService accountService) {
            this.accountService = accountService;
        }

        void setPath(String path) {
            this.path = path;
        }

        String getPath() {
            return path;
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            String path = getPath();
            checkPath(path);
            String[] pathParams = getPathParams(path);
            if (pathParams == null) {
                getAllAccounts(resp);
            } else if (pathParams.length > 1 && String.valueOf(exampleId).equals(pathParams[0])) {
                if ("balance".equals(pathParams[1])) {
                    getBalance(resp);
                } else if ("history".equals(pathParams[1])) {
                    getHistory(resp);
                }
            } else {
                getAccountById(resp);
            }
        }

        private void getAccountById(HttpServletResponse resp) throws IOException {
            try {
                resp.getWriter().println(this.accountService.getAccount(exampleId));
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getHistory(HttpServletResponse resp) throws IOException {
            try {
                resp.getWriter().println(this.accountService.getHistory(exampleId));
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getBalance(HttpServletResponse resp) throws IOException {
            try {
                resp.getWriter().println((Long) this.accountService.getAccount(exampleId).getBalance());
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getAllAccounts(HttpServletResponse resp) throws IOException {
            resp.getWriter().println(this.accountService.getAllAccounts());
        }

        private String[] getPathParams(String path) {
            String[] splitPath = path.split("/");
            if (splitPath.length > 2) {
                return new String[]{splitPath[1], splitPath[2]};
            }
            if (splitPath.length > 1) {
                return new String[]{splitPath[1]};
            }
            return null;
        }

        private void checkPath(String path) {
            String[] splitPath = path.split("/");
            //path for accounts
            if (!splitPath[0].equals(accountsPath)) {
                fail();
            }
        }
    }
}
