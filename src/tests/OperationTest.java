import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Operation;
import org.revolut.model.Person;
import org.revolut.service.OperationService;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OperationTest {
    private static final long exampleId = 1L;
    private static final long exampleAmount = 100L;

    private PrintWriter writer = mock(PrintWriter.class);
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private OperationService operationService = mock(OperationService.class);
    private Operation exampleDepositOperation;
    private Operation exampleWithdrawOperation;
    private Account exampleAccount;

    @Before
    public void setUp() throws InternalLogicException, DataNotFoundException, IOException {
        when(response.getWriter()).thenReturn(writer);
        String deposit = OperationService.OperationType.DEPOSIT.getType();
        String withdraw = OperationService.OperationType.WITHDRAWAL.getType();
        Person examplePerson = Helper.getExamplePerson(exampleId);
        exampleAccount = Helper.getExampleAccount(examplePerson, exampleId);
        exampleDepositOperation = Helper.exampleOperation(deposit, exampleAccount);
        exampleWithdrawOperation = Helper.exampleOperation(withdraw, exampleAccount);
        when(operationService.deposit(anyLong(), anyLong()))
                .thenReturn(exampleDepositOperation);
        when(operationService.withdraw(exampleAccount.getAccountNumber(), exampleAmount))
                .thenReturn(exampleWithdrawOperation);
    }

    @Test
    public void shouldDeposit() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Operation.class);
        TestServlet servlet = new TestServlet(operationService);
        servlet.setPath(exampleAccount.getAccountNumber() + "/deposit/" + exampleAmount);
        servlet.doPost(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(exampleDepositOperation, result);
    }

    @Test
    public void shouldWithdraw() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Operation.class);
        TestServlet servlet = new TestServlet(operationService);
        servlet.setPath(exampleAccount.getAccountNumber() + "/withdrawal/" + exampleAmount);
        servlet.doPost(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(exampleWithdrawOperation, result);
    }

    @WebServlet("/test")
    public static class TestServlet extends HttpServlet {
        private final OperationService operationService;
        private String path = "";
        private HttpServletResponse response;

        @Inject
        public TestServlet(OperationService operationService) {
            this.operationService = operationService;
        }

        void setPath(String path) {
            this.path = path;
        }

        String getPath() {
            return path;
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.response = resp;
            String[] pathParams = getPath().split("/");
            if ("operation".equals(pathParams[0])) {
                getOperation(pathParams[1]);
            } else if ("withdrawal".equals(pathParams[1])) {
                withdraw(pathParams[0], pathParams[2]);
            } else if ("deposit".equals(pathParams[1])) {
                deposit(pathParams[0], pathParams[2]);
            } else if ("transfer".equals(pathParams[1])) {
                transfer(pathParams[0], pathParams[4], pathParams[2]);
            }
        }

        private void transfer(String fromNumber, String toNumber, String amount) throws IOException {
            try {
                response.getWriter().println(
                        operationService.transfer(Long.parseLong(fromNumber),
                                Long.parseLong(toNumber),
                                Long.parseLong(amount)));
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void deposit(String number, String amount) throws IOException {
            try {
                response.getWriter().println(operationService.deposit(Long.parseLong(number), Long.parseLong(amount)));
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void withdraw(String number, String amount) throws IOException {
            try {
                response.getWriter().println(operationService.withdraw(Long.parseLong(number), Long.parseLong(amount)));
            } catch (InternalLogicException | DataNotFoundException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getOperation(String id) throws IOException {
            response.getWriter().println(operationService.getOperation(Long.parseLong(id)));
        }
    }

}
