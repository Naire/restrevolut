import org.revolut.model.Account;
import org.revolut.model.Operation;
import org.revolut.model.Person;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Helper {
    public static final String exampleAccountName = "Example account";
    public static final long exampleBalance = 100L;
    public static final long exampleAccountNumber = 123L;
    public static final String exampleFirstName = "FirstName";
    public static final String exampleLastName = "LastName";
    public static final String exampleUsername = "Username";
    public static final String exampleType = "DEPOSIT";
    public static final Date exampleDate = new Date();

    public static Person getExamplePerson(long id) {
        Person person = new Person();
        person.setId(id);
        person.setFirstName(exampleFirstName);
        person.setLastName(exampleLastName);
        person.setUserName(exampleUsername);
        return person;
    }

    public static Account getExampleAccount(final Person person, long id) {
        Account account = new Account();
        account.setId(id);
        account.setBalance(exampleBalance);
        account.setUser(person);
        account.setAccountName(exampleAccountName);
        account.setAccountNumber(exampleAccountNumber + id);
        return account;
    }

    public static List<Operation> getExampleHistory(Account owner) {
        List<Operation> list = new ArrayList<>();
        list.add(exampleOperation(exampleType, owner));
        return list;
    }

    public static Operation exampleOperation(String type, Account owner) {
        Operation operation = new Operation();
        operation.setType(type);
        operation.setDateOfOperation(exampleDate);
        operation.setAmount(exampleBalance);
        operation.setFromAccount(owner.getId());
        operation.setToAccount(owner.getId());
        operation.setOperationId(owner.getId());
        operation.setOwner(owner);
        return operation;
    }
}
