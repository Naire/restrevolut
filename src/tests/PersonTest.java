import net.bytebuddy.build.ToStringPlugin;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Person;
import org.revolut.service.PersonService;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class PersonTest {
    private static final long exampleId = 1L;
    private static final long exampleAmount = 100L;
    private static final String personsPath = "persons";

    private PrintWriter writer = mock(PrintWriter.class);
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private PersonService personService = mock(PersonService.class);
    private Person examplePerson;
    private Person updatedPerson;
    private List<Person> addedPerson = new ArrayList<>();
    private List<Person> persons = new ArrayList<>();
    private static Person examplePerson2;
    private static Account exampleAccount;

    @Before
    public void setUp() throws InternalLogicException, IOException {
        when(response.getWriter()).thenReturn(writer);

        examplePerson = Helper.getExamplePerson(exampleId);
        examplePerson2 = Helper.getExamplePerson(exampleId + 1);
        Account exampleAccount = Helper.getExampleAccount(examplePerson, exampleId);
        Account exampleAccount2 = Helper.getExampleAccount(examplePerson, exampleId + 1);
        List<Account> accounts = new ArrayList<>();
        accounts.add(exampleAccount);
        accounts.add(exampleAccount2);
        examplePerson.setAccounts(accounts);
        persons.add(examplePerson);
        when(personService.getAllPersons()).thenReturn(persons);
        when(personService.getPerson(examplePerson.getId())).thenReturn(examplePerson);
        addedPerson.add(examplePerson);
        addedPerson.add(examplePerson2);
        when(personService.addPerson(any(Person.class))).thenReturn(addedPerson);
        this.exampleAccount = Helper.getExampleAccount(examplePerson, exampleId + 2);
        when(personService.addAccount(anyLong(), any(Account.class))).thenReturn(examplePerson);
        updatedPerson = Helper.getExamplePerson(exampleId);
        updatedPerson.setUserName("updated");
        updatedPerson.setAccounts(accounts);
        when(personService.updatePerson(any(Person.class))).thenReturn(updatedPerson);
        doNothing().when(personService).removePerson(exampleId);
    }

    @Test
    public void shouldGetAllPersons() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Person.class);
        TestServlet servlet = new TestServlet(personService);
        servlet.setPath(personsPath + "/");
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(persons.toString(), result.toString());
    }

    @Test
    public void shouldGetPersonById() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Person.class);
        TestServlet servlet = new TestServlet(personService);
        servlet.setPath(personsPath + "/" + exampleId);
        servlet.doGet(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(examplePerson, result);
    }

    @Test
    public void shouldAddPerson() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Person.class);
        TestServlet servlet = new TestServlet(personService);
        servlet.setPath(personsPath + "/" + exampleId);
        servlet.doPut(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(updatedPerson, result);
    }

    @Test
    public void shouldAddAccount() throws IOException {
        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Person.class);
        TestServlet servlet = new TestServlet(personService);
        servlet.setPath(personsPath + "/account/" + exampleId);
        servlet.doPut(request, response);
        Mockito.verify(writer).println(captor.capture());
        Object result = captor.getValue();
        assertEquals(examplePerson, result);
    }

    @Test
    public void shouldRemovePerson() throws IOException, InternalLogicException {
        TestServlet servlet = new TestServlet(personService);
        servlet.setPath(personsPath + "/" + exampleId);
        servlet.doDelete(request, response);
        verify(personService).removePerson(exampleId);
    }

    @WebServlet("/test")
    public static class TestServlet extends HttpServlet {
        private final PersonService personService;
        private String path;
        private HttpServletResponse response;

        @Inject
        public TestServlet(PersonService personService) {
            this.personService = personService;
        }

        void setPath(String path) {
            this.path = path;
        }

        String getPath() {
            return path;
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.response = resp;
            String path = getPath();
            checkPath(path);
            addPerson();
        }

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.response = resp;
            String path = getPath();
            String[] pathParams = getPathParams(path);
            if ((personsPath + "/").equals(path)) {
                getAll();
            } else {
                getPerson(Long.parseLong(pathParams[0]));
            }


        }

        @Override
        protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.response = resp;
            String path = getPath();
            checkPath(path);
            String[] pathParams = getPathParams(path);
            if (pathParams.length > 0 && "account".equals(pathParams[0])) {
                addAccount(Long.parseLong(pathParams[1]));
            } else {
                update(Long.parseLong(pathParams[0]));
            }
        }


        @Override
        protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            this.response = resp;
            String path = getPath();
            checkPath(path);
            String[] pathParams = getPathParams(path);
            removePerson(Long.parseLong(pathParams[0]));
        }

        private void update(long id) throws IOException {
            try {
                response.getWriter().println(personService.updatePerson(examplePerson2));
            } catch (InternalLogicException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getPerson(long id) throws IOException {
            try {
                response.getWriter().println(personService.getPerson(id));
            } catch (InternalLogicException e) {
                System.out.println(e.getMessage());
            }
        }

        private void getAll() throws IOException {
            response.getWriter().println(personService.getAllPersons());
        }

        private void addAccount(long id) throws IOException {
            try {
                response.getWriter().println(personService.addAccount(id, exampleAccount));
            } catch (InternalLogicException e) {
                System.out.println(e.getMessage());
            }
        }

        private void removePerson(long id) throws IOException {
            try {
                personService.removePerson(id);
            } catch (InternalLogicException e) {
                System.out.println(e.getMessage());
            }
        }

        private void addPerson() throws IOException {
            try {
                response.getWriter().println(personService.addPerson(examplePerson2));
            } catch (InternalLogicException e) {
                System.out.println(e.getMessage());
            }
        }

        private String[] getPathParams(String path) {
            String[] splitPath = path.split("/");
            if (splitPath.length > 2) {
                return new String[]{splitPath[1], splitPath[2]};
            }
            if (splitPath.length > 1) {
                return new String[]{splitPath[1]};
            }
            return null;
        }

        private void checkPath(String path) {
            String[] splitPath = path.split("/");
            //path for persons
            if (!splitPath[0].equals(personsPath)) {
                fail();
            }
        }

    }
}
