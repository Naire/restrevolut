package org.revolut.controller;

import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Person;
import org.revolut.service.PersonService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonController {

    public static PersonService personService = new PersonService();

    @GET
    public List<Person> getAll() throws IOException {
        return personService.getAllPersons();
    }

    @GET
    @Path("/{id}")
    public Person getPerson(@PathParam("id") long id) throws InternalLogicException, IOException {
        return personService.getPerson(id);
    }

    @POST
    public List<Person> addPerson(Person person) throws InternalLogicException, IOException {
        return personService.addPerson(person);
    }

    @PUT
    @Path("/{id}")
    public Person updatePerson(Person person) throws InternalLogicException, IOException {
        return personService.updatePerson(person);
    }

    @DELETE
    @Path("/{id}")
    public void removePerson(@PathParam("id") long id) throws InternalLogicException, IOException {
        personService.removePerson(id);
    }

    @PUT
    @Path("/account/{personId}")
    public Person addAccount(@PathParam("personId") long id, Account account) throws InternalLogicException, IOException {
        return personService.addAccount(id, account);
    }


}
