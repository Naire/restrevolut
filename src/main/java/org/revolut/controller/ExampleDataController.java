package org.revolut.controller;

import org.revolut.model.Person;
import org.revolut.service.ExampleDataService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.io.IOException;
import java.util.List;

@Path("/")
public class ExampleDataController {

    private static ExampleDataService exampleDataService = new ExampleDataService();

    @GET
    @Path("/exampleData")
    public List<Person> createExampleData() throws IOException {
        return exampleDataService.createExampleData();
    }


}
