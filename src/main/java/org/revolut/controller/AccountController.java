package org.revolut.controller;

import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Operation;
import org.revolut.service.AccountService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountController {

    private static AccountService accountService = new AccountService();

    @GET
    public List<Account> getAllAccounts() throws IOException {
        return accountService.getAllAccounts();
    }

    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long id) throws InternalLogicException, DataNotFoundException, IOException {
        return accountService.getAccount(id);
    }

    @GET
    @Path("/userAccounts/{userId}")
    public List<Account> getUsersAccounts(@PathParam("userId") long id) throws IOException {
        return accountService.getUserAccounts(id);
    }

    @GET
    @Path("{accountId}/history")
    public List<Operation> getHistory(@PathParam("accountId") long id) throws InternalLogicException, DataNotFoundException, IOException {
        return accountService.getHistory(id);
    }

    @GET
    @Path("/{accountId}/balance")
    public String checkBalance(@PathParam("accountId") long accountId) throws InternalLogicException, DataNotFoundException, IOException {
        return String.valueOf(accountService.checkBalance(accountId));
    }
}
