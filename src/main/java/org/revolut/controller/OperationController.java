package org.revolut.controller;

import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Operation;
import org.revolut.service.OperationService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OperationController {
    private static OperationService operationService = new OperationService();

    @POST
    @Path("/{accountNumber}/withdrawal/{amount}")
    public Operation withdraw(@PathParam("accountNumber") long accountNumber, @PathParam("amount") long amount)
            throws InternalLogicException, DataNotFoundException, IOException {
        return operationService.withdraw(accountNumber, amount);
    }

    @POST
    @Path("/{accountNumber}/deposit/{amount}")
    public Operation deposit(@PathParam("accountNumber") long accountNumber, @PathParam("amount") long amount)
            throws InternalLogicException, DataNotFoundException, IOException {
        return operationService.deposit(accountNumber, amount);
    }

    @POST
    @Path("/{fromAccountNumber}/transfer/{amount}/to/{toAccountNumber}")
    public Operation transfer(@PathParam("fromAccountNumber") long fromAccountNumber,
                              @PathParam("toAccountNumber") long toAccountNumber,
                              @PathParam("amount") long amount) throws InternalLogicException, DataNotFoundException, IOException {
        return operationService.transfer(fromAccountNumber, toAccountNumber, amount);
    }

    @GET
    @Path("/operation/{id}")
    public Operation getOperation(@PathParam("id") long id) throws IOException {
        return operationService.getOperation(id);
    }


}
