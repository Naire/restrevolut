package org.revolut.exception;

import org.revolut.model.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InternalLogicExceptionMapper implements ExceptionMapper<InternalLogicException> {
    @Override
    public Response toResponse(InternalLogicException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(), 409);
        return Response.status(Response.Status.CONFLICT).entity(errorMessage).build();
    }
}
