package org.revolut.exception;

public class DataNotFoundException extends Exception {
    final static String message = "Resource not found.";

    public DataNotFoundException() {
        super(message);
    }

    public DataNotFoundException(long id) {
        super(message + " Resource id: " + id);
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
