package org.revolut.exception;

public class InternalLogicException extends Exception {
    public InternalLogicException() {
        super();
    }

    public InternalLogicException(String message) {
        super(message);
    }
}
