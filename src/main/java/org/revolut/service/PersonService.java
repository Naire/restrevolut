package org.revolut.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Person;

import java.io.IOException;
import java.util.List;

public class PersonService {
    final SessionFactory sessionFactory = new Configuration().configure()
            .setProperty("hibernate.show_sql", "true").buildSessionFactory();

    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Person> getAllPersons() throws IOException {
        final Session session = sessionFactory.openSession();
        List list = session.createQuery("FROM Person ").list();
        session.clear();
        session.close();
        return list;
    }

    public Person getPerson(long id) throws InternalLogicException, IOException {
        return checkIdAndGetPerson(id);
    }

    public List<Person> addPerson(Person person) throws IOException, InternalLogicException {
        if (getAllPersons().contains(person)) {
            throw new InternalLogicException("Person already exists!");
        }
        final Session session = sessionFactory.openSession();
        session.save(person);
        session.clear();
        session.close();
        return getAllPersons();
    }

    public Person updatePerson(Person person) throws InternalLogicException, IOException {
        checkIdAndGetPerson(person.getId());
        final Session session = sessionFactory.openSession();
        session.clear();
        session.update(person);
        session.clear();
        session.close();
        return person;
    }

    public Person addAccount(long personId, Account account) throws InternalLogicException, IOException {
        final Session session = sessionFactory.openSession();
        if (session.get(Account.class, account.getId()) != null) {
            session.clear();
            session.close();
            throw new InternalLogicException("Account already exists!");
        }
        Person person = checkIdAndGetPerson(personId);
        account.setUser(person);
        person.getAccounts().add(account);
        session.save(person);
        session.save(account);
        session.clear();
        session.close();
        return person;
    }

    public void removePerson(long id) throws IOException, InternalLogicException {
        Person toRemove = checkIdAndGetPerson(id);
        final Session session = sessionFactory.openSession();
        session.remove(toRemove);
        session.clear();
        session.close();
    }

    private Person checkIdAndGetPerson(long id) throws IOException, InternalLogicException {
        if (id < 0) {
            throw new InternalLogicException("Incorrect id.");
        }
        final Session session = sessionFactory.openSession();
        Person person = session.get(Person.class, id);
        if (person == null) {
            throw new InternalLogicException("Person doesn't exist.");
        }
        session.clear();
        session.close();
        return person;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            sessionFactory.close();
        } finally {
            super.finalize();
        }
    }


}
