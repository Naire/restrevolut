package org.revolut.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Operation;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class AccountService {

    final SessionFactory sessionFactory = new Configuration().configure()
            .setProperty("hibernate.show_sql", "true").buildSessionFactory();

    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Account> getAllAccounts() throws IOException {
        System.out.println("get all");
        Session session = sessionFactory.openSession();
        List list = session.createQuery("From Account ").list();
        session.clear();
        session.close();
        return list;
    }

    public Account getAccount(long id) throws IOException, InternalLogicException, DataNotFoundException {
        return checkIdAndGetAccount(id);
    }

    public List<Account> getUserAccounts(long id) throws IOException {
        Session session = sessionFactory.openSession();
        List list = session.createQuery("From Account where user.id=:id").setParameter("id", id).list();
        session.clear();
        session.close();
        return list;
    }

    public List<Operation> getHistory(long accountId) throws IOException, InternalLogicException, DataNotFoundException {
        checkIdAndGetAccount(accountId);
        Session session = sessionFactory.openSession();
        List list = session.createQuery("From Operation where fromAccount=:id or toAccount=:id order by dateOfOperation")
                .setParameter("id", accountId).list();
        session.clear();
        session.close();
        return list;
    }


    public long checkBalance(long accountId) throws IOException, InternalLogicException, DataNotFoundException {
        if (accountId < 0) {
            throw new InternalLogicException("Arguments are invalid.");
        }
        Session session = sessionFactory.openSession();
        Account account = session.get(Account.class, accountId);
        if (account == null) {
            throw new DataNotFoundException("Account with id " + accountId + " doesn't exist!");
        }
        long balance = account.getBalance();
        Operation operation = new Operation();
        operation.setAmount(0);
        operation.setDateOfOperation(new Date());
        operation.setFromAccount(accountId);
        operation.setToAccount(accountId);
        operation.setType(OperationService.OperationType.BALANCE.getType());
        session.save(operation);
        session.clear();
        session.close();
        return balance;
    }

    private Account checkIdAndGetAccount(long id) throws IOException, InternalLogicException, DataNotFoundException {
        if (id < 0) {
            throw new InternalLogicException("Incorrect id.");
        }
        final Session session = sessionFactory.openSession();
        Account account = session.get(Account.class, id);
        if (account == null) {
            throw new DataNotFoundException("Account doesn't exist.");
        }
        session.clear();
        session.clear();
        return account;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            sessionFactory.close();
        } finally {
            super.finalize();
        }
    }
}
