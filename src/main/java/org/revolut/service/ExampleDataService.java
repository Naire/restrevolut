package org.revolut.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.revolut.model.Account;
import org.revolut.model.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExampleDataService {
    final SessionFactory sessionFactory = new Configuration().configure()
            .setProperty("hibernate.show_sql", "true").buildSessionFactory();

    static {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Person> createExampleData() throws IOException {
        final Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Person> data = new ArrayList<>();
        for (long id = 1; id < 10; id++) {
            Person person = createExamplePerson(id);
            Account account = getExampleAccounts(id, person);
            Account account1 = getExampleAccounts(id + 1, person);
            List<Account> accounts = new ArrayList<>();
            accounts.add(account);
            accounts.add(account1);
            person.setAccounts(accounts);
            session.save(person);
            session.save(account);
            session.save(account1);
            data.add(person);
        }

        session.getTransaction().commit();
        session.clear();
        session.close();
        return data;
    }

    private Person createExamplePerson(long id) {
        Person user = new Person();
        user.setUserName("UserName" + id);
        user.setFirstName("FirstName" + id);
        user.setLastName("LastName" + id);
        return user;
    }

    private Account getExampleAccounts(long id, Person person) {
        Account account = new Account();
        account.setBalance(100 + id);
        account.setAccountNumber(1234 + id);
        account.setAccountName("ExampleAccount " + id);
        account.setUser(person);
        return account;
    }
}
