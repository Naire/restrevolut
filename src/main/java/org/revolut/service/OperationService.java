package org.revolut.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.revolut.exception.DataNotFoundException;
import org.revolut.exception.InternalLogicException;
import org.revolut.model.Account;
import org.revolut.model.Operation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OperationService {
    final SessionFactory sessionFactory = new Configuration().configure()
            .setProperty("hibernate.show_sql", "true").buildSessionFactory();

    public enum OperationType {
        WITHDRAWAL("WITHDRAWAL"),
        DEPOSIT("DEPOSIT"),
        TRANSFER("TRANSFER"),
        BALANCE("BALANCE");

        private String type;

        OperationType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public Operation withdraw(long accountumber, long amount) throws IOException, InternalLogicException, DataNotFoundException {
        List<Account> accounts = checkArgumentsAndAccount(accountumber, accountumber, amount);
        Account account = accounts.get(0);

        long balance = account.getBalance();
        if (balance < amount) {
            throw new InternalLogicException("Balance is too low to withdraw!");
        }

        balance = balance - amount;
        account.setBalance(balance);

        Operation operation = createOperation(account, account, amount, OperationType.WITHDRAWAL.getType());
        transaction(account, operation);

        return operation;
    }

    public Operation deposit(long accountNumber, long amount) throws IOException, InternalLogicException, DataNotFoundException {
        List<Account> accounts = checkArgumentsAndAccount(accountNumber, accountNumber, amount);
        Account account = accounts.get(0);

        long balance = account.getBalance();
        balance = balance + amount;
        account.setBalance(balance);

        Operation operation = createOperation(account, account, amount, OperationType.DEPOSIT.getType());
        transaction(account, operation);
        return operation;
    }

    public Operation transfer(long fromAccountNumber, long toAccountNumber, long amount)
            throws IOException, InternalLogicException, DataNotFoundException {
        List<Account> accounts = checkArgumentsAndAccount(fromAccountNumber, toAccountNumber, amount);
        Account fromAccount = accounts.get(0);
        Account toAccount = accounts.get(1);
        long balanceFrom = fromAccount.getBalance();
        balanceFrom = balanceFrom - amount;
        long balanceTo = toAccount.getBalance();
        balanceTo = balanceTo + amount;
        fromAccount.setBalance(balanceFrom);
        toAccount.setBalance(balanceTo);

        Operation operation = createOperation(fromAccount, toAccount, amount, OperationType.TRANSFER.getType());
        transaction(fromAccount, toAccount, operation);
        return operation;
    }

    public Operation getOperation(long operationId) throws IOException {
        Session session = sessionFactory.openSession();
        Operation operation = session.get(Operation.class, operationId);
        session.clear();
        session.close();
        return operation;
    }

    private List<Account> checkArgumentsAndAccount(long accountNumber1, long accountNumber2, long amount)
            throws IOException, InternalLogicException, DataNotFoundException {
        Session session = sessionFactory.openSession();
        if (amount < 0 || accountNumber1 < 0 || accountNumber2 < 0) {
            throw new InternalLogicException("Arguments are invalid.");
        }
        String query = "From Account where accountNumber=:number";
        Account account1 = (Account) session.createQuery(query)
                .setParameter("number", accountNumber1);
        if (account1 == null) {
            throw new DataNotFoundException("Account with number " + accountNumber1 + " doesn't exist!");
        }
        Account account2 = (Account) session.createQuery(query)
                .setParameter("number", accountNumber2);
        if (account2 == null) {
            throw new DataNotFoundException("Account with number " + accountNumber2 + " doesn't exist!");
        }
        List<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        session.clear();
        session.close();
        return accounts;
    }

    private void transaction(Account account, Operation operation) throws IOException {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(account);
            session.save(operation);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
        } finally {
            session.clear();
            session.close();
        }
    }

    private void transaction(Account account1, Account account2, Operation operation) throws IOException {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(account1);
            session.update(account2);
            session.save(operation);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
        } finally {
            session.clear();
            session.close();
        }
    }

    private Operation createOperation(Account fromAccount, Account toAccount, long amount, String type) throws IOException {
        Operation operation = new Operation();
        operation.setAmount(amount);
        operation.setDateOfOperation(new Date());
        operation.setFromAccount(fromAccount.getId());
        operation.setToAccount(toAccount.getId());
        operation.setType(type);
        return operation;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            sessionFactory.close();
        } finally {
            super.finalize();
        }
    }
}
