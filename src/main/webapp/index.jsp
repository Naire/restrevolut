<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Revoult test app</title>
</head>
<body>
REST API available:
<ul>
    <li><a href="webapi/exampleData"><b>/exampleData</b> - GET - Create example data</a></li>
    <li>----PERSON-----</li>
    <li><a href="webapi/persons"><b>/persons</b> - GET - List all persons</a></li>
    <li><b>webapi/persons/id</b> - GET - Get person by id</li>
    <li><b>webapi/persons</b> - POST - Add new person</li>
    <li><b>webapi/persons/id</b> - PUT - Edit person by id</li>
    <li><b>webapi/persons/id</b> - DELETE - Remove person by id</li>
    <li><b>webapi/persons/account/personId</b> - PUT - Add account to person</li>
    <li>----ACCOUNT-----</li>
    <li><a href="webapi/accounts"><b>/accounts</b> - GET - List all accounts</a></li>
    <li><b>webapi/accounts/id</b> - GET - Get account by id</li>
    <li><b>webapi/accounts/userAccounts/personId</b> - GET - Get person's accounts</li>
    <li><b>webapi/accounts/id/history</b> - GET - Get history of operations for given account</li>
    <li><b>webapi/accounts/id/balance</b> - GET - Get account's balance</li>
    <li>----OPERATION-----</li>
    <li><b>webapi/accountNumber/withdrawal/amount</b> - POST - Withdraw amount from account with given id</li>
    <li><b>webapi/accountNumber/deposit/amount</b> - POST - Deposit amount in account with given id</li>
    <li><b>webapi/fromAccountNumber/transfer/amount/to/toAccountNumber</b> - POST - Transfer amount between accounts
    </li>
    <li><b>webapi/operation/id</b> - GET - Get data of operation by id</li>

</ul>
</body>
</html>
