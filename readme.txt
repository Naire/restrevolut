jar has has embedded Tomcat, to run the app just run the jar.

After running, to have some example data, please open http://localhost:8080/webapi/exampleData - it will generate it.
Available endpoints:
webapi/exampleData - GET - Create example data 
----PERSON-----
webapi/persons - GET - List all persons 
 webapi/persons/id  - GET - Get person by id
 webapi/persons  - POST - Add new person
 webapi/persons/id  - PUT - Edit person by id
 webapi/persons/id  - DELETE - Remove person by id
 webapi/persons/account/personId  - PUT - Add account to person
----ACCOUNT-----
<a href="webapi/accounts"> /accounts  - GET - List all accounts 
 webapi/accounts/id  - GET - Get account by id
 webapi/accounts/userAccounts/personId  - GET - Get person's accounts
 webapi/accounts/id/history  - GET - Get history of operations for given account
 webapi/accounts/id/balance  - GET - Get account's balance
----OPERATION-----
 webapi/accountNumber/withdrawal/amount  - POST - Withdraw amount from account with given id
 webapi/accountNumber/deposit/amount  - POST - Deposit amount in account with given id
 webapi/fromAccountNumber/transfer/amount/to/toAccountNumber  - POST - Transfer amount between accounts
 webapi/operation/id  - GET - Get data of operation by id
 
 ------------------------------TODO-----------------------
 Fix the jsp error.
 Add thread-safety.
 Add tests.